plugins {
    kotlin("jvm") version "1.5.10"
    `maven-publish`
}

group = "com.tsuchinaga.hellokotlinlib"
version = "0.0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
}

tasks {
    test {
        useJUnitPlatform()
    }
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/27272444/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = "zjoqzCZzvRvNe_aKxTn_"
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
