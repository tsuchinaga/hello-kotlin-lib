package com.tsuchinaga.hellokotlinlib

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class HelloWorldFromKotlinTest {

    @Test
    fun greeting() {
        val hw = HelloWorldFromKotlin("Hoge")
        Assertions.assertEquals("Hello, Hoge from Kotlin", hw.greeting())
    }
}
