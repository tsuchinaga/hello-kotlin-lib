package com.tsuchinaga.hellokotlinlib

class HelloWorldFromKotlin(private val name: String) {

    fun greeting(): String {
        return "Hello, %s from Kotlin".format(name)
    }
}
